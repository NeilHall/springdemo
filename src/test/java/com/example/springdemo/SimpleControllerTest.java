package com.example.springdemo;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
class SimpleControllerTest {
    @Mock
    SimpleService simpleServiceMock;

    @InjectMocks
    SimpleController simpleController;

    @Test
    @Description("After calling the simpleService getSum returns a Long that sums the inputs")
    void getSumReturnsALong() {
        when(simpleServiceMock.sumTwoLongs(anyLong(), anyLong())).thenReturn(9L);

        Long result = simpleController.getSum(new ValuesDTO(3L,6L));
        assertThat(result, equalTo(9L));
    }
}
