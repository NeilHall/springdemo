package com.example.springdemo;


import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
//@AllArgsConstructor
public class SimpleController {

    @Autowired
    private SimpleService simpleService;

    @Timed(value = "hello.world", description = "Time taken to return a hello world message")
    @GetMapping(value="/hello", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getHelloWorld() {
        return "Hello World";
    }


    @GetMapping(value="/starwars", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStarWars() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("https://swapi.dev/api/people/1", String.class);
    }


    @GetMapping(value="/morestarwars", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getMoreStarWars(@RequestParam String id) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("https://swapi.dev/api/people/" + id, String.class);
    }


    @PostMapping(value="/sum", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getSum(@RequestBody ValuesDTO valuesDTO) {
        return simpleService.sumTwoLongs(valuesDTO.getValue1(), valuesDTO.getValue2());
    }
}
