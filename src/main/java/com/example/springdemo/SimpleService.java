package com.example.springdemo;

import org.springframework.stereotype.Service;

@Service
public class SimpleService {

    public Long sumTwoLongs(Long value1, Long value2) {
        return value1 + value2;
    }
}
