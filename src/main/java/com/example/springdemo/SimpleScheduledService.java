package com.example.springdemo;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@EnableAsync
public class SimpleScheduledService {

    @Async
    @Scheduled(fixedRate = 1000, initialDelay = 10000)
    void timedLoggingWithInitialDelayAndFixedRateJustForJB() throws InterruptedException {
        System.out.println("Fixed rate task " + new Date());
        Thread.sleep(2000);
    }


    @Scheduled(cron = "*/5 * * * * *", zone = "Europe/London")
    void timedLoggingCron() throws InterruptedException {
        System.out.println("cron every 5 seconds task " + new Date());
        Thread.sleep(2000);
    }
}
