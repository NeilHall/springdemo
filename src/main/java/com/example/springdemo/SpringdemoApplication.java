package com.example.springdemo;

import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringdemoApplication {

    @Bean
    public TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringdemoApplication.class, args);
    }

    //@SpringBootApplication = @Configuration + @ComponentScan + @EnableAutoConfiguration

    //@Configuration
    //This annotation marks a class as a Configuration class for Java-based configuration.
    // This is particularly important if you favor Java-based configuration over XML configuration.

    //@ComponentScan
    //This annotation enables component-scanning so that the web controller classes and other
    // components you create will be automatically discovered and registered as beans in Spring's
    // Application Context. All the@Controller classes you write are discovered by this annotation.
    //
    //@EnableAutoConfiguration
    //This annotation enables the magical auto-configuration feature of Spring Boot, which can
    // automatically configure a lot of stuff for you.
}
