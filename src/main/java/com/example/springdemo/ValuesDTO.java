package com.example.springdemo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValuesDTO {
    Long value1;
    Long value2;
}
